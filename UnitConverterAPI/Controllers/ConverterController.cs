﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnitConverterAPI.Converter.ConverterLogic;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;
using UnitConverterAPI.Converter.ConverterLogic.Implementation;

namespace UnitConverterAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class ConverterController : Controller
	{
		private readonly ILogger<ConverterController> _logger;
		private readonly IConverterHandler _converterHandler;

		public ConverterController(ILogger<ConverterController> logger, IConverterHandler converterHandler)
		{
			_logger = logger;
			_converterHandler = converterHandler;
		}

		[HttpGet]
		public ActionResult<string> Convert(string input, string outputUnit)
		{
			try
			{
				if (_converterHandler.IsConvertable(input, outputUnit, out string reason))
					return _converterHandler.Convert(input, outputUnit);
				else
					return BadRequest($"Request couldn't be processed:\n\t- {reason}");
			}
			catch (Exception ex)
			{
				_logger.LogError($"[{ex.StackTrace}]: {ex.Message}");
				return $"Error occured while converting data:\n\t{ex.Message}";
			}
		}
	}
}
