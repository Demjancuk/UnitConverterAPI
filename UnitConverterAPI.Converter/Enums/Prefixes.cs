﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitConverterAPI.Converter.Enums
{
	enum Prefixes
	{
		yotta = 24,
		zetta = 21,
		exa = 18,
		peta = 15,
		tera = 12,
		giga = 9,
		mega = 6,
		kilo = 3,
		hecto = 2,
		deca = 1,
		none = 0,
		deci = -1,
		centi = -2,
		mili = -3,
		micro = -6,
		nano = -9,
		pico = -12,
		femto = -15,
		atto = -18,
		zepto = -21,
		yocto = -24
	}
}
