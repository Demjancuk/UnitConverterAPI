﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitConverterAPI.Converter.ConverterLogic.Implementation;

namespace UnitConverterAPI.Converter.ConverterLogic.Handlers
{
	public class ConverterHandler : IConverterHandler
	{
		private readonly TemperatureConverter _temperatureConverter = new TemperatureConverter();
		private readonly DataConverter _dataConverter = new DataConverter();
		private readonly LengthConverter _lengthConverter = new LengthConverter();

		public ConverterHandler()
		{ 
		}

		public string Convert(string input, string outputUnit)
		{
			if (_dataConverter.IsConvertable(input, outputUnit, out _, out _, _dataConverter.GetUnits()))
				return _dataConverter.Convert(input, outputUnit, _dataConverter.GetUnits());

			if (_lengthConverter.IsConvertable(input, outputUnit, out _, out _, _lengthConverter.GetUnits()))
				return _lengthConverter.Convert(input, outputUnit, _lengthConverter.GetUnits());

			if (_temperatureConverter.IsConvertable(input, outputUnit, out _, out _, _temperatureConverter.GetUnits()))
				return _temperatureConverter.Convert(input, outputUnit, _temperatureConverter.GetUnits());

			return "There was found no valid conversion!";
		}

		public bool IsConvertable(string input, string outputUnit, out string reason)
		{
			if (input == null || outputUnit == null)
			{
				reason = "input, outputUnit or both of them are null!";
				return false;
			}
			reason = "Valid";
			return true;
		}
	}
}
