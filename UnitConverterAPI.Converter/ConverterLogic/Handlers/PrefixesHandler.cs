﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitConverterAPI.Converter.Enums;

namespace UnitConverterAPI.Converter.ConverterLogic.Handlers
{
	public static class PrefixesHandler
	{
		public static double ConvertNumWithDiffPrefixes(
			double inputNumericalValue, int inputBaseRoot, int outputBaseRoot
		) => inputNumericalValue * Math.Pow(10, inputBaseRoot - outputBaseRoot);

		public static int GetBaseRootOfPrefix(string prefix)
		{
			return Enum.TryParse(typeof(Prefixes), prefix, out object? result) ?
				(int)result : 0;
		}

		public static string GetPrefixNameByBaseRoot(int baseRoot)
			=> Enum.GetName(typeof(Prefixes), baseRoot);
	}
}
