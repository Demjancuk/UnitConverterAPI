﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitConverterAPI.Converter.ConverterLogic.Handlers
{
	public interface IConverterHandler
	{
		public string Convert(string input, string outputUnit);
		public bool IsConvertable(string input, string outputUnit, out string reason);
	}
}
