﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;

namespace UnitConverterAPI.Converter.ConverterLogic.Implementation
{
	public class TemperatureConverter : BaseConverter
	{
		// 0C = 32F
		// 1C = 33.8F

		public new string Convert(string input, string outputUnit, IDictionary<string, double> multipliers = null, int[] bases = null)
		{
			if (bases == null)
			{
				bases = GetBasesOfNumbersFromStrings(input, outputUnit);
			}

			string[] splittedInput = input.Split(' ');
			string siPrefixToRemoveInput = PrefixesHandler.GetPrefixNameByBaseRoot(bases[0]) ?? "none";
			string siPrefixToRemoveOutput = PrefixesHandler.GetPrefixNameByBaseRoot(bases[1]) ?? "none";

			return MakeConversion(
				double.Parse(splittedInput[0]),
				bases[0],
				multipliers.Where(x => input.Contains(x.Key)).FirstOrDefault().Value,
				bases[1],
				multipliers.Where(x => outputUnit.Contains(x.Key)).FirstOrDefault().Value,
				outputUnit,
				32
			);
		}

		public IDictionary<string, double> GetUnits()
		{
			return new Dictionary<string, double>
			{
				{ "celsius" , 1 },
				{ "fahrenheit" , 1.8 }
			};
		}
	}
}
