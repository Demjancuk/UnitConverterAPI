﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;
using UnitConverterAPI.Converter.Enums;

namespace UnitConverterAPI.Converter.ConverterLogic.Implementation
{
	public partial class BaseConverter : IConverter
	{
		public BaseConverter()
		{
		}

		public string Convert(string input, string outputUnit, IDictionary<string, double> multipliers = null, int[] bases = null)
		{
			if (bases == null)
			{
				bases = GetBasesOfNumbersFromStrings(input, outputUnit);
			}

			string[] splittedInput = input.Split(' ');
			string siPrefixToRemoveInput = PrefixesHandler.GetPrefixNameByBaseRoot(bases[0]) ?? "none";
			string siPrefixToRemoveOutput = PrefixesHandler.GetPrefixNameByBaseRoot(bases[1]) ?? "none";

			return MakeConversion(
				double.Parse(splittedInput[0]),
				bases[0],
				multipliers.Where(x=> input.Contains(x.Key)).FirstOrDefault().Value,
				bases[1],
				multipliers.Where(x => outputUnit.Contains(x.Key)).FirstOrDefault().Value,
				outputUnit
			);
		}

		protected bool IsInRightFormat(string input, string unit, out string prefix)
		{
			prefix = null;
			string[] splittedPhrases = input.Split(' ');
			if (splittedPhrases.Length == 2)
			{
				if (!double.TryParse(splittedPhrases[0], out _))
					return false;
				string possiblePrefix = prefix = splittedPhrases[1].Replace(unit, "");
				return
					Enum.GetNames(typeof(Prefixes)).ToList().Any(x => x == possiblePrefix) ||
					possiblePrefix.Length == 0;
			}
			else
			{
				string possiblePrefix = prefix = input.Replace(unit, "");
				return
					Enum.GetNames(typeof(Prefixes)).ToList().Any(x => x == possiblePrefix) ||
					possiblePrefix.Length == 0;
			}
		}

		public int[] GetBasesOfNumbersFromStrings(string input, string outputUnit)
			=> new int[]
				{
					GetBaseOfPrefixFromString(input),
					GetBaseOfPrefixFromString(outputUnit)
				};

		public IDictionary<string, double> GetUnits()
		{
			throw new NotImplementedException();
		}

		public bool IsConvertable(string input, string outputUnit, out string reason)
		{
			if (input == null || outputUnit == null)
			{
				reason = "One or both of variables are null";
				return false;
			}
			reason = "Success";
			return true;
		}

		public bool IsConvertable(string input, string outputUnit, out string reason, out int[] bases, IDictionary<string, double> units)
		{
			bool inputValid = false, outputValid = false;
			reason = "Success";
			bases = new int[2];
			foreach (string unit in units.Keys)
			{
				if (input.Contains(unit) && IsInRightFormat(input, unit, out string prefixInput))
				{
					inputValid = true;
					bases[0] = PrefixesHandler.GetBaseRootOfPrefix(prefixInput);
					if (outputValid)
						return true;
				}
				if (outputUnit.Contains(unit) && IsInRightFormat(outputUnit, unit, out string prefixOutput))
				{
					outputValid = true;
					bases[1] = PrefixesHandler.GetBaseRootOfPrefix(prefixOutput);
					if (inputValid)
						return true;
				}
			}
			reason = "Input(s) are non convertable!";
			return false;
		}

		public int GetBaseOfPrefixFromString(string prefixWithUnit)
		{
			foreach (string prefix in Enum.GetNames(typeof(Prefixes)))
			{
				if (prefixWithUnit.Contains(prefix))
					return (int)(Prefixes)Enum.Parse(typeof(Prefixes), prefix);
			}
			return 0;
		}

		public string MakeConversion(
			double inputValue, int inputBaseRoot, double inputMultiplier, 
			int outputBaseRoot, double outputMultiplier, string outputUnit, 
			double differentialStart = 0)
		{
			double convertedValue = PrefixesHandler.ConvertNumWithDiffPrefixes(
				inputValue / inputMultiplier * outputMultiplier, inputBaseRoot, outputBaseRoot) + differentialStart;
			return $"{convertedValue} {outputUnit}";
		}
	}
}
