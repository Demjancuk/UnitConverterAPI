﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitConverterAPI.Converter.ConverterLogic.Implementation
{
	public class LengthConverter : BaseConverter
	{
		public IDictionary<string, double> GetUnits()
		{
			return new Dictionary<string, double>
			{
				{ "meter" , 1 },
				{ "feet" , 3.2808399 },
				{ "inch" , 39.3700787 },
			};
		}
	}
}
