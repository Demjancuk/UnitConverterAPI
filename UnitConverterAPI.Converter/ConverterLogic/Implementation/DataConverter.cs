﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;

namespace UnitConverterAPI.Converter.ConverterLogic.Implementation
{
	public class DataConverter : BaseConverter
	{
		public new IDictionary<string, double> GetUnits()
		{
			return new Dictionary<string, double>
			{
				{ "bit" , 8 },
				{ "byte" , 1 }
			};
		}
	}
}
