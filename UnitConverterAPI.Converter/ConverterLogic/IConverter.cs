﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitConverterAPI.Converter.ConverterLogic
{
	public interface IConverter
	{
		/// <summary>
		/// Get units of type with convert proportion
		/// </summary>
		/// <returns>Collection of KeyValuePair<string type, double convertProportion></returns>
		public IDictionary<string, double> GetUnits();

		/// <summary>
		/// Converts data to required output
		/// </summary>
		/// <param name="input">Input value</param>
		/// <param name="outputUnit">Required output with format</param>
		/// <param name="bases">Inputs and outputs base root of exponent ten in array by index respectively</param>
		/// <returns>Converted data as string</returns>
		public string Convert(string input, string outputUnit, IDictionary<string, double> multipliers, int[] bases);

		/// <summary>
		/// Tells if input and output are egible for conversion
		/// </summary>
		/// <param name="input">input to convert</param>
		/// <param name="outputUnit">output unit to convert input into</param>
		/// <returns>true - if entered values are convertable</returns>
		public bool IsConvertable(string input, string outputUnit, out string reason);

		/// <summary>
		/// Tells if input and output are egible for conversion
		/// </summary>
		/// <param name="input">input to convert</param>
		/// <param name="outputUnit">output unit to convert input into</param>
		/// <param name="reason">Reason why it is not convertable if it is not</param>
		/// <param name="bases">Inputs and outputs base root of exponent ten in array by index respectively</param>
		/// <param name="units">Units from given converter given from outside</param>
		/// <returns>true - if entered values are convertable</returns>
		public bool IsConvertable(string input, string outputUnit, out string reason, out int[] bases, IDictionary<string, double> units);

		/// <summary>
		///	Converts data to required output with specified values and containes function for evaluation of all conversions
		/// </summary>
		/// <param name="inputValue">Inputs numerical value</param>
		/// <param name="inputBaseRoot">Inputs base root of exponent ten</param>
		/// <param name="inputMultiplier">Inputs multiplier against base unit</param>
		/// <param name="outputValue">Outputs numerical value</param>
		/// <param name="outputMultiplier">Outputs multiplier against base unit</param>
		/// <param name="differentialStart">Value to add if scales of units dont start at the same point (example: °C » °F = +32)</param>
		/// <returns>Converted value in string format</returns>
		public string MakeConversion(
			double inputValue, int inputBaseRoot, double inputMultiplier,
			int outputBaseRoot, double outputMultiplier, string outputUnit,
			double differentialStart);
	}
}
