﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;

namespace UnitConverterAPI.Tests
{
	public class ConverterTests
	{
		IConverterHandler _converterHandler;

		[SetUp]
		public void Setup()
		{
			_converterHandler = new ConverterHandler();
		}

		private bool IsSameToOneToMilionthOfInput(string input, string output)
		{
			try
			{
				double inputNum = double.Parse(input.Replace('.', ',').Split(' ')[0]);
				double outputNum = double.Parse(output.Replace('.', ',').Split(' ')[0]);

				double oneTrilionthOfSmaller = inputNum / 1000000;

				return
					inputNum > 0 ?
						inputNum - outputNum < oneTrilionthOfSmaller :
						inputNum + outputNum < oneTrilionthOfSmaller;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		[Test]
		public void Convert_byte_to_bit()
		{
			const string INPUT_CONVERT_FROM = "1784 byte";
			const string INPUT_CONVERT_TO = "bit";
			const string EXPECTED_OUTPUT = "14272 bit";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT, 
					result
				)
			);
		}

		[Test]
		public void Convert_bit_to_byte()
		{
			const string INPUT_CONVERT_FROM = "14272 bit";
			const string INPUT_CONVERT_TO = "byte";
			const string EXPECTED_OUTPUT = "1784 byte";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}

		[Test]
		public void Convert_meter_to_feet()
		{
			const string INPUT_CONVERT_FROM = "14272 meter";
			const string INPUT_CONVERT_TO = "feet";
			const string EXPECTED_OUTPUT = "46824.147 feet";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}
		[Test]
		public void Convert_meter_to_inch()
		{
			const string INPUT_CONVERT_FROM = "14272 meter";
			const string INPUT_CONVERT_TO = "inch";
			const string EXPECTED_OUTPUT = "561889.764 inch";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}

		[Test]
		public void Convert_feet_to_inch()
		{
			const string INPUT_CONVERT_FROM = "14272 feet";
			const string INPUT_CONVERT_TO = "inch";
			const string EXPECTED_OUTPUT = "171264 inch";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}

		[Test]
		public void Convert_celsius_to_fahrenheit()
		{
			const string INPUT_CONVERT_FROM = "14272 celsius";
			const string INPUT_CONVERT_TO = "fahrenheit";
			const string EXPECTED_OUTPUT = "25721.6 fahrenheit";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}

		[Test]
		public void Convert_fahrenheit_to_celsius()
		{
			const string INPUT_CONVERT_FROM = "57821 fahrenheit";
			const string INPUT_CONVERT_TO = "celsius";
			const string EXPECTED_OUTPUT = "32105 celsius";

			string result = _converterHandler.Convert(
				INPUT_CONVERT_FROM,
				INPUT_CONVERT_TO
			);

			Assert.IsTrue(
				IsSameToOneToMilionthOfInput(
					EXPECTED_OUTPUT,
					result
				)
			);
		}
	}
}
