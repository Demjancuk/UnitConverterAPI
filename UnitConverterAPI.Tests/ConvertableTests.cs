﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using UnitConverterAPI.Converter.ConverterLogic.Handlers;

namespace UnitConverterAPI.Tests
{
	public class ConvertableTests
	{
		IConverterHandler _converterHandler;

		[SetUp]
		public void Setup()
		{
			_converterHandler = new ConverterHandler();
		}

		[Test]
		public void IsConvertable_bit()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bit", "bit", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bits", "bit", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bit", "bits", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bits", "bits", out _)
			);
		}

		[Test]
		public void IsConvertable_byte()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 byte", "byte", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bytes", "byte", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 byte", "bytes", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 bytes", "bytes", out _)
			);
		}

		[Test]
		public void IsConvertable_celsius()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 celsius", "celsius", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 celsiuses", "celsius", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 celsius", "celsiuses", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 celsiuses", "celsiuses", out _)
			);
		}

		[Test]
		public void IsConvertable_fahrenheit()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 fahrenheit", "fahrenheit", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 fahrenheites", "fahrenheit", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 fahrenheit", "fahrenheites", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 fahrenheites", "fahrenheites", out _)
			);
		}

		[Test]
		public void IsConvertable_inch()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 inch", "inch", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 inches", "inch", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 inch", "inches", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 inches", "inches", out _)
			);
		}

		[Test]
		public void IsConvertable_feet()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 feet", "feet", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 feets", "feet", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 feet", "feets", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 feets", "feets", out _)
			);
		}

		[Test]
		public void IsConvertable_meter()
		{
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 meter", "meter", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 meters", "meter", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 meter", "meters", out _)
			);
			Assert.AreEqual(
				true,
				_converterHandler.IsConvertable("1784 meters", "meters", out _)
			);
		}
	}
}
